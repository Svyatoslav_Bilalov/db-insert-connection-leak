create table public.test_table
(
    first_id  integer not null,
    second_id integer not null,
    test_str  varchar not null,
    primary key (first_id, second_id)
);