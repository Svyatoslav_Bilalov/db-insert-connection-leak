package com.example.config;

import io.r2dbc.spi.ConnectionFactory;
import org.jooq.DSLContext;
import org.jooq.impl.DSL;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;

@SpringBootConfiguration
public class AppConfig {

  @Bean
  public DSLContext dslContext(ConnectionFactory connectionFactory) {
    return DSL.using(connectionFactory);
  }

}
