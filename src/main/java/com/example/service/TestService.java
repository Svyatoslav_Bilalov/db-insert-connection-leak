package com.example.service;

import static com.example.jooq.db.public_.Tables.TEST_TABLE;

import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class TestService {

  private final DSLContext dslContext;

  @Autowired
  public TestService(DSLContext dslContext) {
    this.dslContext = dslContext;
  }

  public Mono<Void> test() {
    return Mono.from(
        dslContext
            .insertInto(TEST_TABLE)
            .set(TEST_TABLE.FIRST_ID, 1)
            .set(TEST_TABLE.SECOND_ID, 1)
            .set(TEST_TABLE.TEST_STR, "test")
            .returning()
    ).then();
  }

}
